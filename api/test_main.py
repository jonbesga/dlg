import json
import pytest

from api.server import app as flask_app


@pytest.fixture
def client():
    flask_app.config['TESTING'] = True

    with flask_app.test_client() as client:
        yield client

def test_total_endpoint_correct_data(client):
    numbers_list = list(range(10000001))
    numbers_list_sum = sum(numbers_list)
    resp = client.post('/total/', json=numbers_list)
    resp = json.loads(resp.data)
    assert numbers_list_sum == resp["total"]

def test_total_endpoint_no_json_mimetype(client):
    data = "foo"
    resp = client.post('/total/', data=data)
    assert resp.status_code == 400

def test_total_endpoint_no_list(client):
    data = {"foo": "bar"}
    resp = client.post('/total/', json=data)
    assert resp.status_code == 400

def test_total_endpoint_malformed_json(client):
    data = '{"bar"}'
    resp = client.post('/total/', data=data, headers={"Content-Type": "application/json"})
    assert resp.status_code == 400

def test_total_endpoint_not_all_numbers(client):
    data = ["a", 3, 5, 7]
    resp = client.post('/total/', json=data)
    assert resp.status_code == 400