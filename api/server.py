
import flask

from flask import Flask, request, jsonify
from werkzeug.exceptions import BadRequest


app = Flask(__name__)

@app.route('/total/', methods=["POST"])
def total():
  if not request.is_json:
    return jsonify({"error": "Please make sure the mimetype indicates JSON data, either application/json or application/*+json"}), 400

  try:
    numbers = request.get_json()
    assert isinstance(numbers, list)
    total = sum(numbers)
    response, status_code = {"total": total}, 200
  except BadRequest:
    response, status_code = {"error": "invalid json data"}, 400
  except AssertionError:
    response, status_code = {"error": "provided data should be a list"}, 400
  except TypeError:
    response, status_code = {"error": "all elements from the list should be integer or float numbers"}, 400

  return jsonify(response), status_code