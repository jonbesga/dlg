# DLG

## Run

`export FLASK_APP=api.server`

`flask run`

## Train of thought

As the test requires to keep the libraries to a minimum and is a simple endpoint, I have decided to use Flask as web framework (the test description also hints to use it showing the request example using the port 5000) and pytest for testing.

Regarding the setup, I usually use Poetry to manage virtual environments but I exported a requirements.txt for those who don't use this tool. If you haven't heard of it I encourage you to check it, it's really useful and easy to use.

The server code is a simple endpoint. Among all the possible choices to send the data, I have decided to use a POST method and making sure that the data provided to the endpoint complies with the following assumptions:

* Is JSON data
* Is valid JSON data
* The data is a list and all the elements from the list are summable

When one of these conditions is not achieved. The endpoint will return a 400 JSON response with a useful error message.

Please note that in the tests I am just checking that the error code is 400 and not the content of the error messages for all the cases where the sum of the numbers is not doable. I think the error message is a friendly guide for external parties using the API of what they are doing wrong when interacting with the endpoint.
