#!/bin/bash

URL=http://localhost:5000/total/

curl -X POST ${URL}  -d '[1,2,3,4,5,6,7,8,9]'

curl -X POST ${URL}  -d '[1,2,3,4,5,6,7,8,9]' -H "Content-Type: application/json"

curl -X POST ${URL}  -d '{asd}' -H "Content-Type: application/json"

curl -X POST ${URL}  -d '{"foo":"bar"}' -H "Content-Type: application/json"

curl -X POST ${URL}  -d '[1,"asd",3,4,5,6,7,8,9]' -H "Content-Type: application/json"

